<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

use BadMethodCallException;
use Dromos\Exceptions\DuplicateServiceException;
use Dromos\Exceptions\UnknownServiceException;

class App
{
	protected array $services = [];

	public function __isset(string $name): bool
	{
		return isset($this->services[$name]);
	}

	public function __get(string $name)
	{
		if (!isset($this->services[$name])) {
			throw new UnknownServiceException("Unknown service {$name}");
		}
		$service = $this->services[$name];

		return $service();
	}

	public function __call(string $method, array $args)
	{
		if (!isset($this->services[$method])
		 || !is_callable($this->services[$method])) {
			throw new BadMethodCallException("Unknown method {$method}()");
		}

		return call_user_func_array($this->services[$method], $args);
	}

	public function register(string $name, callable $closure)
	{
		if (isset($this->services[$name])) {
			throw new DuplicateServiceException("A service is already registered under {$name}");
		}

		$this->services[$name] = function () use ($closure) {
			static $instance;
			if (null === $instance) {
				$instance = $closure();
			}

			return $instance;
		};
	}
}
