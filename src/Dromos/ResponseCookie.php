<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

class ResponseCookie
{
	public function __construct(
		public readonly string $name,
		public readonly ?string $value = null,
		public readonly ?int $expire = null,
		public readonly ?string $path = null,
		public readonly ?string $domain = null,
		public readonly bool $secure = false,
		public readonly bool $http_only = false
	) {
	}
}
