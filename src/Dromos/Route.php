<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

class Route
{
	protected $callback;

	public function __construct(callable &$callback, public readonly string $path, public readonly ?string $method = null, public readonly string $namespace = '')
	{ // NOTE: Match all methods if null
		$this->setCallback($callback);
	}

	public function &callback(): callable
	{
		return $this->callback;
	}

	public function setCallback(callable &$callback): void
	{
		$this->callback = &$callback;
	}
}
