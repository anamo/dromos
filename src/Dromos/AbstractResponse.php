<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

use Dromos\DataCollection\HeaderDataCollection;
use Dromos\DataCollection\ResponseCookieDataCollection;
use Dromos\Exceptions\ResponseAlreadySentException;

abstract class AbstractResponse
{
	public readonly ResponseCookieDataCollection $cookies;

	public readonly HeaderDataCollection $headers;

	protected static int $default_status_code = 200;

	protected string $protocol_version = '1.1';

	// protected mixed $body;

	protected HttpStatus $status;

	protected bool $sent = false;

	public function __construct(protected mixed $body = null, ?int $status_code = null, ?array $headers = [])
	{
		$status_code = $status_code ?: static::$default_status_code;

		$this->body($body);
		$this->code($status_code);

		$this->headers = new HeaderDataCollection($headers ?? []);
		$this->cookies = new ResponseCookieDataCollection();
	}

	public function protocolVersion(?string $protocol_version = null): string
	{
		if (null !== $protocol_version) {
			$this->requireUnlocked();

			$this->protocol_version = (string) $protocol_version;
		}

		return $this->protocol_version;
	}

	public function &body(mixed $body = null): mixed
	{
		if (null !== $body) {
			$this->requireUnlocked();

			$this->body = $body;
		}

		return $this->body;
	}

	public function &status(): HttpStatus
	{
		return $this->status;
	}

	public function code(?int $code = null): int
	{
		if (null !== $code) {
			$this->requireUnlocked();

			$this->status = new HttpStatus($code);
		}

		return $this->status->code;
	}

	public function requireUnlocked(): self
	{
		if ($this->isSent()) {
			throw new ResponseAlreadySentException('Response has already been sent');
		}

		return $this;
	}

	public function sendHeaders(bool $cookies_also = true, bool $override = false): self
	{
		if (headers_sent()
		 && !$override) {
			return $this;
		}

		header($this->httpStatusLine());

		foreach ($this->headers as $key => $value) {
			header($key.': '.$value, false);
		}

		if ($cookies_also) {
			$this->sendCookies($override);
		}

		return $this;
	}

	public function sendCookies(bool $override = false): self
	{
		if (headers_sent()
		&& !$override) {
			return $this;
		}

		foreach ($this->cookies as $cookie) {
			setcookie(
				$cookie->name,
				$cookie->value,
				$cookie->expire,
				$cookie->path,
				$cookie->domain,
				$cookie->secure,
				$cookie->http_only
			);
		}

		return $this;
	}

	public function sendBody(): self
	{
		echo $this->body;

		return $this;
	}

	public function send(bool $override = false): self
	{
		if ($this->sent
		 && !$override) {
			throw new ResponseAlreadySentException('Response has already been sent');
		}

		$this->sendHeaders();
		$this->sendBody();

		$this->sent = true;

		// If there running FPM, tell the process manager to finish the server request/response handling
		if (function_exists('fastcgi_finish_request')) {
			fastcgi_finish_request();
		}

		return $this;
	}

	public function isSent(): bool
	{
		return $this->sent;
	}

	public function header(string $key, string $value): self
	{
		$this->headers->set($key, $value);

		return $this;
	}

	public function cookie(
		string $key,
		string $value = '',
		int $expiry = 0,
		string $path = '/',
		string $domain = '',
		bool $secure = false,
		bool $httponly = false
	): self {
		if (0 === $expiry) {
			$expiry = time() + (3600 * 24 * 30);
		}

		$this->cookies->set(
			$key,
			new ResponseCookie($key, $value, $expiry, $path, $domain, $secure, $httponly)
		);

		return $this;
	}

	public function noCache(): self
	{
		$this->header('Pragma', 'no-cache');
		$this->header('Cache-Control', 'no-store, no-cache');

		return $this;
	}

	public function redirect(string $url, int $code = 302): self
	{
		$this->code($code);
		$this->header('Location', $url);

		return $this;
	}

	protected function httpStatusLine(): string
	{
		return sprintf('HTTP/%s %s', $this->protocol_version, $this->status);
	}
}
