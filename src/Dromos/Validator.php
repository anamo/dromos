<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

use BadMethodCallException;
use Dromos\Exceptions\ValidationException;

class Validator
{
	public static array $methods = [];

	protected readonly string|bool $err;

	protected static bool $default_added = false;

	public function __construct(protected readonly mixed $str, null|string|bool $err = null)
	{
		$this->err = is_null($err) ? '' : $err;

		if (!static::$default_added) {
			static::addDefault();
		}
	}

	public function __call(string $method, array $args)
	{
		$reverse = false;
		$validator = $method;
		$method_substr = substr($method, 0, 2);

		if ('is' === $method_substr) {       // is<$validator>()
			$validator = substr($method, 2);
		} elseif ('no' === $method_substr) { // not<$validator>()
			$validator = substr($method, 3);
			$reverse = true;
		}

		$validator = strtolower($validator);

		if (!$validator || !isset(static::$methods[$validator])) {
			throw new BadMethodCallException("Unknown method {$method}()");
		}

		$validator = static::$methods[$validator];
		array_unshift($args, $this->str);

		switch (count($args)) {
			case 1:
				$result = $validator($args[0]);

				break;

			case 2:
				$result = $validator($args[0], $args[1]);

				break;

			case 3:
				$result = $validator($args[0], $args[1], $args[2]);

				break;

			case 4:
				$result = $validator($args[0], $args[1], $args[2], $args[3]);

				break;

			default:
				$result = call_user_func_array($validator, $args);

				break;
		}

		$result = (bool) ($result ^ $reverse);

		if (false === $this->err) {
			return $result;
		}
		if (false === $result) {
			throw new ValidationException($this->err);
		}

		return $this;
	}

	public static function addDefault(): void
	{
		static::$methods['empty'] = function (mixed $str): bool {
			return empty($str);
		};
		static::$methods['null'] = function (mixed $str) {
			return null === $str;
		};
		static::$methods['len'] = function (string $str, int $min, ?int $max = null) {
			$len = strlen($str);

			return null === $max ? $len === $min : $len >= $min && $len <= $max;
		};
		static::$methods['date'] = function (string $str, string $format = 'Y-m-d') {
			$d = \DateTime::createFromFormat($format, $str);

			return $d && $d->format($format) === $str;
		};
		static::$methods['pastdate'] = function (string $str, string $format = 'Y-m-d') {
			$d = \DateTime::createFromFormat($format, $str);

			return $d && $d->format($format) === $str && $d < new \DateTime();
		};
		static::$methods['futuredate'] = function (string $str, string $format = 'Y-m-d') {
			$d = \DateTime::createFromFormat($format, $str);

			return $d && $d->format($format) === $str && $d > new \DateTime();
		};
		static::$methods['gt'] = function (float|int $str, float|int $min) {
			return $min < $str;
		};
		static::$methods['gte'] = function (float|int $str, float|int $min) {
			return $min <= $str;
		};
		static::$methods['lt'] = function (float|int $str, float|int $min) {
			return $min > $str;
		};
		static::$methods['lte'] = function (float|int $str, float|int $min) {
			return $min >= $str;
		};
		static::$methods['int'] = function (mixed $str) {
			return (string) $str === ((string) (int) $str);
		};
		static::$methods['float'] = function (mixed $str) {
			return (string) $str === ((string) (float) $str);
		};
		static::$methods['email'] = function (mixed $str) {
			return false !== filter_var($str, FILTER_VALIDATE_EMAIL);
		};
		static::$methods['url'] = function (mixed $str) {
			return false !== filter_var($str, FILTER_VALIDATE_URL);
		};
		static::$methods['ip'] = function (mixed $str) {
			return false !== filter_var($str, FILTER_VALIDATE_IP);
		};
		static::$methods['remoteip'] = function (mixed $str) {
			return false !== filter_var($str, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
		};
		static::$methods['alnum'] = function (string $str) {
			return ctype_alnum($str);
		};
		static::$methods['alpha'] = function (string $str) {
			return ctype_alpha($str);
		};
		static::$methods['contains'] = function (string $str, mixed $needle) {
			return false !== strpos($str, $needle);
		};
		static::$methods['regex'] = function (string $str, string $pattern) {
			return preg_match($pattern, $str);
		};
		static::$methods['chars'] = function (string $str, $chars) {
			return preg_match("/^[{$chars}]++$/i", $str);
		};

		static::$default_added = true;
	}

	public static function addValidator(string $method, callable $callback): void
	{
		static::$methods[strtolower($method)] = $callback;
	}
}
