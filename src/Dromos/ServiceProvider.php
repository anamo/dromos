<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

use Dromos\DataCollection\DataCollection;

class ServiceProvider
{
	protected ?Request $request = null;

	protected ?AbstractResponse $response = null;

	protected $session_id;

	protected string $layout;

	protected string $view;

	protected DataCollection $shared_data;

	public function __construct(Request &$request = null, AbstractResponse &$response = null)
	{
		// Bind our objects
		$this->bind($request, $response);

		// Instantiate our shared data collection
		$this->shared_data = new DataCollection();
	}

	public function __isset(string $key): bool
	{
		return $this->shared_data->exists($key);
	}

	public function __get(string $key)
	{
		return $this->shared_data->get($key);
	}

	public function __set(string $key, $value): void
	{
		$this->shared_data->set($key, $value);
	}

	public function __unset(string $key): void
	{
		$this->shared_data->remove($key);
	}

	public function bind(Request &$request = null, AbstractResponse &$response = null): self
	{
		$this->request = $request ?: $this->request;
		$this->response = $response ?: $this->response;

		return $this;
	}

	public function &sharedData(): DataCollection
	{
		return $this->shared_data;
	}

	public function startSession()
	{
		if ('' === session_id()) {
			// Attempt to start a session
			session_start();

			$this->session_id = session_id() ?: false;
		}

		return $this->session_id;
	}

	public function flash(string $msg, string $type = 'info', ?array $params = null): void
	{
		$this->startSession();
		if (is_array($type)) {
			$params = $type;
			$type = 'info';
		}
		if (!isset($_SESSION['__flashes'])) {
			$_SESSION['__flashes'] = [$type => []];
		} elseif (!isset($_SESSION['__flashes'][$type])) {
			$_SESSION['__flashes'][$type] = [];
		}
		$_SESSION['__flashes'][$type][] = $this->markdown($msg, $params);
	}

	public function flashes($type = null): array
	{
		$this->startSession();

		if (!isset($_SESSION['__flashes'])) {
			return [];
		}

		if (null === $type) {
			$flashes = $_SESSION['__flashes'];
			unset($_SESSION['__flashes']);
		} else {
			$flashes = [];
			if (isset($_SESSION['__flashes'][$type])) {
				$flashes = $_SESSION['__flashes'][$type];
				unset($_SESSION['__flashes'][$type]);
			}
		}

		return $flashes;
	}

	public static function markdown(string $str, $args = null): string
	{
		// Create our markdown parse/conversion regex's
		$md = [
			'/\[([^\]]++)\]\(([^\)]++)\)/' => '<a href="$2">$1</a>',
			'/\*\*([^\*]++)\*\*/' => '<strong>$1</strong>',
			'/\*([^\*]++)\*/' => '<em>$1</em>',
		];

		// Let's make our arguments more "magical"
		$args = func_get_args(); // Grab all of our passed args
		$str = array_shift($args); // Remove the initial arg from the array (and set the $str to it)
		if (isset($args[0]) && is_array($args[0])) {
			/**
			 * If our "second" argument (now the first array item is an array)
			 * just use the array as the arguments and forget the rest.
			 */
			$args = $args[0];
		}

		// Encode our args so we can insert them into an HTML string
		foreach ($args as &$arg) {
			$arg = htmlentities($arg, ENT_QUOTES, 'UTF-8');
		}

		// Actually do our markdown conversion
		return vsprintf(preg_replace(array_keys($md), $md, $str), $args);
	}

	public static function escape(string $str, int $flags = ENT_QUOTES): string
	{
		return htmlentities($str, $flags, 'UTF-8');
	}

	public function refresh(): self
	{
		$this->response->redirect(
			$this->request->uri()
		);

		return $this;
	}

	public function back(): self
	{
		$referer = $this->request->server->get('HTTP_REFERER');

		if (null !== $referer) {
			$this->response->redirect($referer);
		} else {
			$this->refresh();
		}

		return $this;
	}

	/**
	 * Get (or set) the view's layout.
	 *
	 * Simply calling this method without any arguments returns the current layout.
	 * Calling with an argument, however, sets the layout to what was provided by the argument.
	 *
	 * @param string $layout The layout of the view
	 *
	 * @return ServiceProvider|string
	 */
	public function layout($layout = null)
	{
		if (null !== $layout) {
			$this->layout = $layout;

			return $this;
		}

		return $this->layout;
	}

	/**
	 * Renders the current view.
	 */
	public function yieldView()
	{
		require $this->view;
	}

	/**
	 * Renders a view + optional layout.
	 *
	 * @param string $view The view to render
	 * @param array  $data The data to render in the view
	 */
	public function render($view, array $data = [])
	{
		$original_view = $this->view;

		if (!empty($data)) {
			$this->shared_data->merge($data);
		}

		$this->view = $view;

		if (null === $this->layout) {
			$this->yieldView();
		} else {
			require $this->layout;
		}

		// if (false !== $this->response->chunked) {
		// 	$this->response->chunk();
		// }

		// restore state for parent render()
		$this->view = $original_view;
	}

	/**
	 * Renders a view without a layout.
	 *
	 * @param string $view The view to render
	 * @param array  $data The data to render in the view
	 */
	public function partial($view, array $data = [])
	{
		$layout = $this->layout;
		$this->layout = null;
		$this->render($view, $data);
		$this->layout = $layout;
	}

	public function addValidator(string $method, callable $callback): void
	{
		Validator::addValidator($method, $callback);
	}

	public function validate(mixed $string, ?string $err = null): Validator
	{
		return new Validator($string, $err);
	}

	public function validateParam(string $param, ?string $err = null): Validator
	{
		return $this->validate($this->request->param($param), $err);
	}
}
