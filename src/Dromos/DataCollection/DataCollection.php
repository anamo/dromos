<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\DataCollection;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;

class DataCollection implements IteratorAggregate, ArrayAccess, Countable
{
	protected array $attributes = [];

	public function __construct(array $attributes = [])
	{
		$this->attributes = $attributes;
	}

	public function __get(string $key)
	{
		return $this->get($key);
	}

	public function __set(string $key, $value): void
	{
		$this->set($key, $value);
	}

	public function __isset(string $key): bool
	{
		return $this->exists($key);
	}

	public function __unset(string $key): void
	{
		$this->remove($key);
	}

	public function keys(?array $mask = null, bool $fill_with_nulls = true): array
	{
		if (null !== $mask) {
			// Support a more "magical" call
			if (!is_array($mask)) {
				$mask = func_get_args();
			}

			/*
			 * Make sure that the returned array has at least the values
			 * passed into the mask, since the user will expect them to exist
			 */
			if ($fill_with_nulls) {
				$keys = $mask;
			} else {
				$keys = [];
			}

			/*
			 * Remove all of the values from the keys
			 * that aren't in the passed mask
			 */
			return array_intersect(
				array_keys($this->attributes),
				$mask
			) + $keys;
		}

		return array_keys($this->attributes);
	}

	public function all(?array $mask = null, bool $fill_with_nulls = true): array
	{
		if (null !== $mask) {
			// Support a more "magical" call
			if (!is_array($mask)) {
				$mask = func_get_args();
			}

			/*
			 * Make sure that each key in the mask has at least a
			 * null value, since the user will expect the key to exist
			 */
			if ($fill_with_nulls) {
				$attributes = array_fill_keys($mask, null);
			} else {
				$attributes = [];
			}

			/*
			 * Remove all of the keys from the attributes
			 * that aren't in the passed mask
			 */
			return array_intersect_key(
				$this->attributes,
				array_flip($mask)
			) + $attributes;
		}

		return $this->attributes;
	}

	public function get(string $key, $default_val = null)
	{
		if (isset($this->attributes[$key])) {
			return $this->attributes[$key];
		}

		return $default_val;
	}

	public function set(string $key, $value): self
	{
		$this->attributes[$key] = $value;

		return $this;
	}

	public function replace(array $attributes = []): self
	{
		$this->attributes = $attributes;

		return $this;
	}

	public function merge(array $attributes = [], bool $hard = false): self
	{
		// Don't waste our time with an "array_merge" call if the array is empty
		if (!empty($attributes)) {
			// Hard merge?
			if ($hard) {
				$this->attributes = array_replace(
					$this->attributes,
					$attributes
				);
			} else {
				$this->attributes = array_merge(
					$this->attributes,
					$attributes
				);
			}
		}

		return $this;
	}

	public function exists(string $key): bool
	{
		// Don't use "isset", since it returns false for null values
		return array_key_exists($key, $this->attributes);
	}

	public function remove(string $key)
	{
		unset($this->attributes[$key]);
	}

	public function clear(): self
	{
		return $this->replace();
	}

	public function isEmpty(): bool
	{
		return empty($this->attributes);
	}

	public function cloneEmpty(): self
	{
		$clone = clone $this;
		$clone->clear();

		return $clone;
	}

	public function getIterator(): ArrayIterator
	{
		return new ArrayIterator($this->attributes);
	}

	public function offsetGet(mixed $key): mixed
	{
		return $this->get($key);
	}

	public function offsetSet($key, $value): void
	{
		$this->set($key, $value);
	}

	public function offsetExists($key): bool
	{
		return $this->exists($key);
	}

	public function offsetUnset($key): void
	{
		$this->remove($key);
	}

	public function count(): int
	{
		return count($this->attributes);
	}
}
