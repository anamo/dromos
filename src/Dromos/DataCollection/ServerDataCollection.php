<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\DataCollection;

/**
 * ServerDataCollection.
 *
 * A DataCollection for "$_SERVER" like data
 *
 * Look familiar?
 *
 * Inspired by @fabpot's Symfony 2's HttpFoundation
 *
 * @see https://github.com/symfony/HttpFoundation/blob/master/ServerBag.php
 */
class ServerDataCollection extends DataCollection
{
	/**
	 * Class properties.
	 */

	/**
	 * The prefix of HTTP headers normally
	 * stored in the Server data.
	 */
	protected static string $http_header_prefix = 'HTTP_';

	/**
	 * The list of HTTP headers that for some
	 * reason aren't prefixed in PHP...
	 */
	protected static array $http_nonprefixed_headers = [
		'CONTENT_LENGTH',
		'CONTENT_TYPE',
		'CONTENT_MD5',
	];

	public static function hasPrefix(string $string, string $prefix): bool
	{
		if (0 === strpos($string, $prefix)) {
			return true;
		}

		return false;
	}

	public function getHeaders(): array
	{
		// Define a headers array
		$headers = [];

		foreach ($this->attributes as $key => $value) {
			// Does our server attribute have our header prefix?
			if (self::hasPrefix($key, self::$http_header_prefix)) {
				// Add our server attribute to our header array
				$headers[
					substr($key, strlen(self::$http_header_prefix))
				] = $value;
			} elseif (in_array($key, self::$http_nonprefixed_headers, true)) {
				// Add our server attribute to our header array
				$headers[$key] = $value;
			}
		}

		return $headers;
	}
}
