<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\DataCollection;

class HeaderDataCollection extends DataCollection
{
	/**
	 * Normalization option.
	 *
	 * Don't normalize
	 *
	 * @var int
	 */
	public const NORMALIZE_NONE = 0;

	/**
	 * Normalization option.
	 *
	 * Normalize the outer whitespace of the header
	 *
	 * @var int
	 */
	public const NORMALIZE_TRIM = 1;

	/**
	 * Normalization option.
	 *
	 * Normalize the delimiters of the header
	 *
	 * @var int
	 */
	public const NORMALIZE_DELIMITERS = 2;

	/**
	 * Normalization option.
	 *
	 * Normalize the case of the header
	 *
	 * @var int
	 */
	public const NORMALIZE_CASE = 4;

	/**
	 * Normalization option.
	 *
	 * Normalize the header into canonical format
	 *
	 * @var int
	 */
	public const NORMALIZE_CANONICAL = 8;

	/**
	 * Normalization option.
	 *
	 * Normalize using all normalization techniques
	 *
	 * @var int
	 */
	public const NORMALIZE_ALL = -1;

	/**
	 * Properties.
	 */

	/**
	 * The header key normalization technique/style to
	 * use when accessing headers in the collection.
	 */
	public int $normalization = self::NORMALIZE_ALL;

	/**
	 * Methods.
	 *
	 * @param mixed $normalization
	 */

	/**
	 * Constructor.
	 *
	 * @override (doesn't call our parent)
	 *
	 * @param array $headers       The headers of this collection
	 * @param int   $normalization The header key normalization technique/style to use
	 */
	public function __construct(array $headers = [], int $normalization = self::NORMALIZE_ALL)
	{
		$this->normalization = $normalization;

		foreach ($headers as $key => $value) {
			$this->set($key, $value);
		}
	}

	public function get(string $key, $default_val = null)
	{
		$key = $this->normalizeKey($key);

		return parent::get($key, $default_val);
	}

	public function set(string $key, $value): self
	{
		$key = $this->normalizeKey($key);

		return parent::set($key, $value);
	}

	public function exists(string $key): bool
	{
		$key = $this->normalizeKey($key);

		return parent::exists($key);
	}

	public function remove(string $key): void
	{
		$key = $this->normalizeKey($key);

		parent::remove($key);
	}

	/**
	 * Normalize a header key's delimiters.
	 *
	 * This will convert any space or underscore characters
	 * to a more standard hyphen (-) character
	 *
	 * @param string $key The ("field") key of the header
	 */
	public static function normalizeKeyDelimiters(string $key): string
	{
		return str_replace([' ', '_'], '-', $key);
	}

	/**
	 * Canonicalize a header key.
	 *
	 * The canonical format is all lower case except for
	 * the first letter of "words" separated by a hyphen
	 *
	 * @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
	 *
	 * @param string $key The ("field") key of the header
	 */
	public static function canonicalizeKey(string $key): string
	{
		$words = explode('-', strtolower($key));

		foreach ($words as &$word) {
			$word = ucfirst($word);
		}

		return implode('-', $words);
	}

	/**
	 * Normalize a header name by formatting it in a standard way.
	 *
	 * This is useful since PHP automatically capitalizes and underscore
	 * separates the words of headers
	 *
	 * @todo Possibly remove in future, here for backwards compatibility
	 *
	 * @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
	 *
	 * @param string $name           The name ("field") of the header
	 * @param bool   $make_lowercase Whether or not to lowercase the name
	 *
	 * @deprecated Use the normalization options and the other normalization methods instead
	 */
	public static function normalizeName(string $name, bool $make_lowercase = true): string
	{
		// Warn user of deprecation
		trigger_error(
			'Use the normalization options and the other normalization methods instead.',
			E_USER_DEPRECATED
		);

		/*
		 * Lowercasing header names allows for a more uniform appearance,
		 * however header names are case-insensitive by specification
		 */
		if ($make_lowercase) {
			$name = strtolower($name);
		}

		// Do some formatting and return
		return str_replace(
			[' ', '_'],
			'-',
			trim($name)
		);
	}

	/**
	 * Normalize a header key based on our set normalization style.
	 *
	 * @param string $key The ("field") key of the header
	 *
	 * @return string
	 */
	protected function normalizeKey($key)
	{
		if ($this->normalization & static::NORMALIZE_TRIM) {
			$key = trim($key);
		}

		if ($this->normalization & static::NORMALIZE_DELIMITERS) {
			$key = static::normalizeKeyDelimiters($key);
		}

		if ($this->normalization & static::NORMALIZE_CASE) {
			$key = strtolower($key);
		}

		if ($this->normalization & static::NORMALIZE_CANONICAL) {
			$key = static::canonicalizeKey($key);
		}

		return $key;
	}
}
