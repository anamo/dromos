<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\DataCollection;

use Dromos\ResponseCookie;

class ResponseCookieDataCollection extends DataCollection
{
	public function __construct(array $cookies = [])
	{
		foreach ($cookies as $key => $value) {
			$this->set($key, $value);
		}
	}

	public function set(string $key, $value): self
	{
		if (!$value instanceof ResponseCookie) {
			$value = new ResponseCookie($key, $value);
		}

		return parent::set($key, $value);
	}
}
