<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\DataCollection;

use Dromos\Route;

/**
 * RouteCollection.
 *
 * A DataCollection for Routes
 */
class RouteCollection extends DataCollection
{
	/**
	 * Methods.
	 */

	/**
	 * Constructor.
	 *
	 * @override (doesn't call our parent)
	 *
	 * @param array $routes The routes of this collection
	 */
	public function __construct(array $routes = [])
	{
		foreach ($routes as $value) {
			$this->add($value);
		}
	}

	/**
	 * Set a route.
	 *
	 * {@inheritdoc}
	 *
	 * A value may either be a callable or a Route instance
	 * Callable values will be converted into a Route with
	 * the "name" of the route being set from the "key"
	 *
	 * A developer may add a named route to the collection
	 * by passing the name of the route as the "$key" and an
	 * instance of a Route as the "$value"
	 *
	 * @see DataCollection::set()
	 *
	 * @param string         $key   The name of the route to set
	 * @param callable|Route $value The value of the route to set
	 *
	 * @return RouteCollection
	 */
	public function set(string $key, $value): self
	{
		// if (!$value instanceof Route) {
		// 	$value = new Route($value);
		// }

		return parent::set($key, $value);
	}

	/**
	 * Add a route instance to the collection.
	 *
	 * This will auto-generate a name
	 *
	 * @return RouteCollection
	 */
	public function addRoute(Route $route): self
	{
		/**
		 * Auto-generate a name from the object's hash
		 * This makes it so that we can autogenerate names
		 * that ensure duplicate route instances are overridden.
		 */
		$name = spl_object_hash($route);

		return $this->set($name, $route);
	}

	/**
	 * Add a route to the collection.
	 *
	 * This allows a more generic form that
	 * will take a Route instance, string callable
	 * or any other Route class compatible callback
	 *
	 * @return RouteCollection
	 */
	public function add(Route|callable $route): self
	{
		// if (!$route instanceof Route) {
		// 	$route = new Route($route);
		// }

		return $this->addRoute($route);
	}
}
