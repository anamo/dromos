<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

use Dromos\DataCollection\RouteCollection;
use Throwable;

class Dromos
{
	// protected array $routes;

	// protected RouteFactory $route_factory;

	public readonly Request $request;

	// protected Response $response;

	// protected ServiceProvider $service;

	// protected App $app;

	private bool $skip_remaining = false;

	private $last_handler;

	public function __construct(
		?Request $request = null,
		public readonly Response $response = new Response(),
		public readonly ServiceProvider $service = new ServiceProvider(),
		public readonly App $app = new App(),
		public readonly RouteCollection $routes = new RouteCollection(),
		protected readonly RouteFactory $route_factory = new RouteFactory()
	) {
		$this->request = $request ?? Request::createFromGlobals();
		$this->service->bind($this->request, $this->response); // NOTE: Service binds to req/response AFTER dispatch.
	}

	public function __call($name, $arguments): void
	{
		extract(
			$this->parseLooseArgumentOrder($arguments),
			EXTR_OVERWRITE
		);

		$this->respond(mb_strtoupper($name), $path, $callback);
	}

	public function respond($method = null, $path = null, $callback = null): void
	{
		extract(
			$this->parseLooseArgumentOrder(func_get_args()),
			EXTR_OVERWRITE
		);

		$route = $this->route_factory->build($callback, $path, $method);

		$this->routes->addRoute($route);
	}

	public function with(string $namespace, callable $routes): void
	{
		$previous = $this->route_factory->namespace;

		$this->route_factory->namespace .= $namespace;

		call_user_func_array($routes, [&$this]);

		$this->route_factory->namespace = $previous;
	}

	public function onError(
		callable $callback
	): void {
		$this->last_handler = &$callback;
		set_exception_handler(fn (Throwable $ex) => call_user_func_array($callback, [&$ex, &$this]));
	}

	public function afterDispatch(
		callable $callback
	): void {
		register_shutdown_function(function () use (&$callback) {
			try {
				call_user_func_array($callback, [&$this, &$this->request, &$this->response, &$this->service, &$this->app]);
			} catch (Throwable $ex) {
				call_user_func_array($this->last_handler, [&$ex, &$this]);
			}
		});
	}

	public function dispatch(): void
	{
		$namespace_routes = array_filter($this->routes->all(), fn ($v) => 0 === @substr_compare($this->request->pathname(), $v->namespace, 0, strlen($v->namespace)));

		// array_walk($namespace_routes, function ($v) {
		// 	var_dump(1111, $v->path, $v->method);
		// });

		// $after_routes = array_filter($namespace_routes, fn (&$v) => is_null($v->path) && (is_null($v->method)
		// || $v->method === $this->request->method()));
		// $shutdown = function ($callback) {
		// 	call_user_func_array($callback, [&$this, &$this->request, &$this->response, &$this->service, &$this->app]);
		// };
		// array_walk($after_routes, fn ($v) => register_shutdown_function($shutdown, $v->callback()));

		$specific_routes = array_filter($namespace_routes, fn ($v) => !is_null($v->path)
		&& (is_null($v->method)
		|| $v->method === $this->request->method()));

		$specific_routes = array_filter($specific_routes, function ($v) {
			preg_match($v->path, mb_substr($this->request->pathname(), mb_strlen($v->namespace)), $output_array, PREG_OFFSET_CAPTURE);
			if (is_array($output_array)) {
				$params_array = array_filter($output_array, fn ($k) => is_string($k), ARRAY_FILTER_USE_KEY);
				$params_array = array_map(fn ($v) => reset($v), $params_array);
				foreach ((array) $params_array as $param_key => $param_value) {
					$this->request->{$param_key} = $param_value;
				}
			}

			return !empty($output_array);
		});

		foreach ($specific_routes as $route) {
			if ($this->skip_remaining) {
				break;
			}

			call_user_func_array($route->callback(), [&$this->request, &$this->response, &$this->service, &$this->app]);
		}
	}

	public function skipRemaining(): void
	{
		$this->skip_remaining = true;
	}

	protected function parseLooseArgumentOrder(array $args): array
	{
		$callback = array_pop($args);
		$path = array_pop($args);
		$method = array_pop($args);

		return [
			'method' => $method,
			'path' => $path,
			'callback' => $callback,
		];
	}
}
