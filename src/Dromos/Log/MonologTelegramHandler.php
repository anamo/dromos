<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Log;

use Dromos\Exceptions\MissingExtensionException;
use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Handler\Curl\Util;
use Monolog\Logger;
use Monolog\LogRecord;

class MonologTelegramHandler extends AbstractProcessingHandler
{
	protected const TELEGRAM_LOG_HOST = 'https://api.telegram.org';

	/**
	 * @param string     $bot        Datadog Api Key access
	 * @param array      $attributes Some options fore Datadog Logs
	 * @param int|string $level      The minimum logging level at which this handler will be triggered
	 * @param bool       $bubble     Whether the messages that are handled can bubble up the stack or not
	 */
	public function __construct(
		protected string $bot,
		protected string $chat,
		$level = Logger::DEBUG,
		bool $bubble = true
	) {
		if (!extension_loaded('curl')) {
			throw new MissingExtensionException('The curl extension is needed to use the DatadogHandler');
		}

		$this->bot = $bot;
		$this->chat = $chat;

		parent::__construct($level, $bubble);
	}

	protected function write(LogRecord $record): void
	{
		$this->send($record['formatted']);
	}

	protected function send(string $record): void
	{
		curl_setopt_array($ch = curl_init(), [
			CURLOPT_URL => self::TELEGRAM_LOG_HOST.'/'.$this->bot.'/sendMessage?'.http_build_query(['chat_id' => $this->chat, 'text' => $record]),
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => 1,
			CURLOPT_TIMEOUT => 1,
		]);

		Util::execute($ch);
	}

	protected function getDefaultFormatter(): FormatterInterface
	{
		return new JsonFormatter();
	}
}
