<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Exceptions;

use OverflowException;

/**
 * DuplicateServiceException.
 *
 * Exception used for when a service is attempted to be registered that already exists
 */
class DuplicateServiceException extends OverflowException implements DromosExceptionInterface
{
}
