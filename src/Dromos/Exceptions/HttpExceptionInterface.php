<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Exceptions;

interface HttpExceptionInterface extends DromosExceptionInterface
{
	public function __construct();
}
