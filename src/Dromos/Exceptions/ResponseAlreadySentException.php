<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Exceptions;

use RuntimeException;

/**
 * ResponseAlreadySentException.
 *
 * Exception used for when a response is attempted to be sent after its already been sent
 */
class ResponseAlreadySentException extends RuntimeException implements DromosExceptionInterface
{
}
