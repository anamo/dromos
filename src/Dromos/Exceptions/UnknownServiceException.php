<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Exceptions;

use OutOfBoundsException;

/**
 * UnknownServiceException.
 *
 * Exception used for when a service was called that doesn't exist
 */
class UnknownServiceException extends OutOfBoundsException implements DromosExceptionInterface
{
}
