<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Exceptions;

use UnexpectedValueException;

/**
 * ValidationException.
 *
 * Exception used for Validation errors
 */
class ValidationException extends UnexpectedValueException implements DromosExceptionInterface
{
}
