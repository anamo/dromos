<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Exceptions;

/**
 * DromosExceptionInterface.
 *
 * Exception interface that Dromos's exceptions should implement
 *
 * This is mostly for having a simple, common Interface class/namespace
 * that can be type-hinted/instance-checked against, therefore making it
 * easier to handle Dromos exceptions while still allowing the different
 * exception classes to properly extend the corresponding SPL Exception type
 */
interface DromosExceptionInterface
{
}
