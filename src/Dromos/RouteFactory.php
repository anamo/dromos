<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

class RouteFactory
{
	public const EMPTY_PATH_VALUE = '/^.*$/';

	public string $namespace = '';

	public function build(callable &$callback, ?string $path = null, ?string $method = null): Route
	{
		return new Route(
			$callback,
			$this->preprocess_path_string($path),
			is_string($method) ? mb_strtoupper($method) : null,
			$this->namespace,
		);
	}

	protected function preprocess_path_string(?string $path = null): string
	{
		return !empty($path) ? (string) $path : static::EMPTY_PATH_VALUE;
	}
}
