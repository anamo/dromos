<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

use Dromos\DataCollection\DataCollection;
use Dromos\DataCollection\HeaderDataCollection;
use Dromos\DataCollection\ServerDataCollection;

class Request
{
	public readonly DataCollection $paramsNamed;

	public readonly DataCollection $paramsGet;

	public readonly DataCollection $paramsPost;

	public readonly DataCollection $cookies;

	public readonly ServerDataCollection $server;

	public readonly HeaderDataCollection $headers;

	public readonly DataCollection $files;

	protected string $id;

	protected ?string $body;

	final public function __construct(
		array $params_get = [],
		array $params_post = [],
		array $cookies = [],
		array $server = [],
		array $files = [],
		?string $body = null
	) {
		$this->paramsGet = new DataCollection($params_get);
		$this->paramsPost = new DataCollection($params_post);
		$this->cookies = new DataCollection($cookies);
		$this->server = new ServerDataCollection($server);
		$this->headers = new HeaderDataCollection($this->server->getHeaders());
		$this->files = new DataCollection($files);
		$this->body = $body ? (string) $body : null;

		$this->paramsNamed = new DataCollection();
	}

	public function __isset(string $param): bool
	{
		$params = $this->params();

		return isset($params[$param]);
	}

	public function __get(string $param)
	{
		return $this->param($param);
	}

	public function __set(string $param, $value): void
	{
		$this->paramsNamed->set($param, $value);
	}

	public function __unset(string $param): void
	{
		$this->paramsNamed->remove($param);
	}

	public static function createFromGlobals(): static
	{
		return new static(
			$_GET,
			$_POST,
			$_COOKIE,
			$_SERVER,
			$_FILES,
			null
		);
	}

	public function id(bool $hash = true): string
	{
		if (null === $this->id) {
			$this->id = uniqid();

			if ($hash) {
				$this->id = sha1($this->id);
			}
		}

		return $this->id;
	}

	public function &body(): string
	{
		if (null === $this->body) {
			$this->body = @file_get_contents('php://input');
		}

		return $this->body;
	}

	public function params(?array $mask = null, bool $fill_with_nulls = true): array
	{
		if (null !== $mask
		&& $fill_with_nulls) {
			$attributes = array_fill_keys($mask, null);
		} else {
			$attributes = [];
		}

		return array_merge(
			$attributes,
			$this->paramsGet->all($mask, false),
			$this->paramsPost->all($mask, false),
			$this->cookies->all($mask, false),
			$this->paramsNamed->all($mask, false) // Add our named params last
		);
	}

	public function param(string $key, mixed $default = null): mixed
	{
		$params = $this->params();

		return isset($params[$key]) ? $params[$key] : $default;
	}

	public function isSecure(): bool
	{
		return true == $this->server->get('HTTPS');
	}

	public function ip(): string
	{
		return $this->server->get('REMOTE_ADDR');
	}

	public function userAgent(): string
	{
		return $this->headers->get('USER_AGENT');
	}

	public function uri(): string
	{
		return $this->server->get('REQUEST_URI', '/');
	}

	public function pathname(): string
	{
		$uri = $this->uri();

		return strstr($uri, '?', true) ?: $uri; // Strip the query string from the URI
	}

	public function method(?string $is = null, bool $allow_override = true): string|bool
	{
		$method = $this->server->get('REQUEST_METHOD', 'GET');

		// Overrides
		if ($allow_override
		&& 'POST' === $method) {
			if ($this->server->exists('X_HTTP_METHOD_OVERRIDE')) { // For legacy servers, override the HTTP method with the X-HTTP-Method-Override header or _method parameter
				$method = $this->server->get('X_HTTP_METHOD_OVERRIDE', $method);
			} else {
				$method = $this->param('_method', $method);
			}

			$method = strtoupper($method);
		}

		if (null !== $is) { // We're doing a check
			return 0 === strcasecmp($method, $is);
		}

		return $method;
	}

	public function query(string $key, $value = null): string
	{
		$query = [];

		parse_str(
			$this->server->get('QUERY_STRING'),
			$query
		);

		if (is_array($key)) {
			$query = array_merge($query, $key);
		} else {
			$query[$key] = $value;
		}

		$request_uri = $this->uri();

		if (false !== strpos($request_uri, '?')) {
			$request_uri = strstr($request_uri, '?', true);
		}

		return $request_uri.(!empty($query) ? '?'.http_build_query($query) : null);
	}
}
