<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos;

use Dromos\Exceptions\ResponseAlreadySentException;
use RuntimeException;

class Response extends AbstractResponse
{
	public function file(string $path, ?string $filename = null, ?string $mimetype = null): self
	{
		if ($this->sent) {
			throw new ResponseAlreadySentException('Response has already been sent');
		}

		$this->body('');
		$this->noCache();

		if (null === $filename) {
			$filename = basename($path);
		}
		if (null === $mimetype) {
			$mimetype = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);
		}

		$this->header('Content-type', $mimetype);
		$this->header('Content-Disposition', 'attachment; filename="'.$filename.'"');

		$this->header('Content-length', filesize($path));

		$this->sendHeaders();

		$bytes_read = readfile($path);

		if (false === $bytes_read) {
			throw new RuntimeException('The file could not be read');
		}

		$this->sendBody();

		$this->sent = true;

		// If there running FPM, tell the process manager to finish the server request/response handling
		if (function_exists('fastcgi_finish_request')) {
			fastcgi_finish_request();
		}

		return $this;
	}

	public function json($object, ?string $jsonp_prefix = null): self
	{
		$this->body('');
		$this->noCache();

		$json = json_encode($object);

		if (null !== $jsonp_prefix) {
			$this->header('Content-Type', 'text/javascript');
			$this->body("{$jsonp_prefix}({$json});");
		} else {
			$this->header('Content-Type', 'application/json');
			$this->body($json);
		}

		$this->send();

		return $this;
	}
}
