<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

use Dromos\Dromos;
use Dromos\Log\MonologTelegramHandler;
use Monolog\Logger;

(function () {
	$path = '/';
	while (!($autoloader = @include __DIR__."{$path}/vendor/autoload.php")) {
		$path .= '../';
	}
	// $autoloader = require __DIR__.'/../vendor/autoload.php';
	// $autoloader->add('Dromos\Tests', __DIR__);
	// spl_autoload_register(function ($class_name) {
	// 	$CLASSES_DIR = __DIR__.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR; // or whatever your directory is
	// 	$file = $CLASSES_DIR.$class_name.'.php';
	// 	if (file_exists($file)) {
	// 		include $file;
	// 	}
	// 	// only include if file exists, otherwise we might enter some conflicts with other pieces of code which are also using the spl_autoload_register function
	// });
})();

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
	throw new ErrorException(@$errstr, 0, @$errno, @$errfile, @$errline);
});

// OPTIONAL
set_exception_handler($bootstrap_exception_handler = function (Throwable $ex) {
	curl_setopt_array($ch = curl_init(), [
		CURLOPT_URL => 'https://api.telegram.org/bot178153406:AAEmBXf4gjvO577bp25eBXKhDCPnKyBJ0Iw/sendMessage?'.http_build_query(['chat_id' => '178163713', 'text' => $ex->getMessage()]),
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_SSL_VERIFYPEER => 1,
		CURLOPT_TIMEOUT => 1,
	]);
	$output = curl_exec($ch);
	curl_close($ch);
});

$dromos = new Dromos();

$dromos->onError(function (Throwable &$ex, &$dromos) use (&$bootstrap_exception_handler) {
	try {
		$dromos->response->body('<p>Error</p>');

		// log
		$dromos->app->log->error($ex->getMessage());
	} catch (Throwable $logger_ex) { // NOTE: Logger error
		restore_exception_handler(); // OPTIONAL
		$bootstrap_exception_handler($logger_ex); // OPTIONAL
	} finally {
		// output
		$dromos->response->code(500);
		$dromos->response->send(true);
	}
});

$dromos->app->register('log', function () {
	$log = new Logger('telegram-logger');

	include 'file_with_errors.php';
	$log->pushHandler(new MonologTelegramHandler('bot178153406:AAEmBXf4gjvO577bp25eBXKhDCPnKyBJ0Iw', '178163713', Logger::DEBUG));

	return $log;
});

include 'file_with_errors.php';

$dromos->afterDispatch(function (&$dromos, &$request, &$response, &$service, &$app) {
	// log
	$app->log->info($response->status());
});

$dromos->afterDispatch(function (&$dromos, &$request, &$response) {
	if ($response->isSent()) {
		return;
	}

	if (empty($response->body())) {
		$response->body('404');
		$response->code(404);
	}

	$response->send();
});

$dromos->respond(function (&$request, &$response, &$service, &$app) {
	$app->log->info('Registered logger');
});

/*$dromos->afterDispatch(function ($dromos, &$request, &$response) {
			if ($response->isSent()) {
				return;
			}
			// var_dump('9shutdown');
			if (empty($response->body())) {
				$response->body('<h1>404</h1>');
				$response->code(404);
			}

			$response->send();
		});*/

$dromos->afterDispatch(function (&$dromos, &$request, &$response) {
	if ($response->isSent()) {
		return;
	}
	// $response->body('7777');

	// if (!$response->isSent()) {
	// 	$response->send();
	// }
	// var_dump('7shutdown', $request->PHPSESSID);
});

$dromos->afterDispatch(function (&$dromos, &$request, &$response) {
	if ($response->isSent()) {
		return;
	}
	// var_dump('8shutdown', $request->PHPSESSID);
});

// include 'file_with_errors.php';
$dromos->with('/foo', function () use (&$dromos) {
	$dromos->with('/bar', function () use (&$dromos) {
		$dromos->respond('/^\/hello$/', function (&$request, &$response) {
			// var_dump(1111);
		});

		$dromos->respond('/^.*$/', function (&$request, &$response, &$service, &$app) {
			// $response->body('1111');
			// $response->lock();
			// var_dump(1111);

			// var_dump('7shutdown', $request->PHPSESSID);
			$renderer = include 'build/test.php';
			$service->name = 'John';
			$response->body($renderer($service->sharedData()->all()));
		});

		$dromos->respond('GET', '/^\/hello$/', function (&$request, &$response) {
			// var_dump(2222);
		});

		$dromos->head(function (&$request, &$response) {
			// var_dump(1111);
		});

		$dromos->get('/^\/hello$/', function (&$request, &$response) {
			// var_dump(2222);
		});

		$dromos->respond('/^\/hello\/(?<router_resource>\w+)$/', function (&$request, &$response) {
			var_dump($request->params());
			// trigger_error('This is a trigger_error message', E_USER_WARNING);
			// throw new ErrorException('This is a ErrorException message', 0, E_USER_WARNING);
		});

		$dromos->respond('/^\/hello\/(?<router_resource>\w+)$/', function (&$request, &$response) {
			var_dump(6666);
		});
		// $dromos->respond('GET', '/[:id]', function (&$request, &$response) {
		// 	// Show a single user
		// });
	});
});

// var_DUMP($dromos);

$dromos->dispatch();
