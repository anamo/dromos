<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

// Set some configuration values
ini_set('session.use_cookies', 0);      // Don't send headers when testing sessions
ini_set('session.cache_limiter', '');   // Don't send cache headers when testing sessions

// Load our autoloader, and add our Test class namespace
$autoloader = require __DIR__.'/../vendor/autoload.php';
$autoloader->add('Dromos\Tests', __DIR__);

// Load our functions bootstrap
// require __DIR__.'/functions-bootstrap.php';
