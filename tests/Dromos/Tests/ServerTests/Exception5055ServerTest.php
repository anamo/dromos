<?php

declare(strict_types=1);
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Tests\ServerTests;

use Dromos\Tests\AbstractDromosServerTest;

/**
 * @internal
 * @coversNothing
 */
final class Exception5055ServerTest extends AbstractDromosServerTest
{
	public function test500a()
	{
		curl_setopt_array($ch = curl_init('http://localhost:'.self::$port.'/exc'), [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 3,
		]);
		$response = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$this->assertEquals('500', $response);
		$this->assertEquals(500, $httpcode);
	}

	public function test500b()
	{
		curl_setopt_array($ch = curl_init('http://localhost:'.self::$port.'/err'), [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 3,
		]);
		$response = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$this->assertEquals('500', $response);
		$this->assertEquals(500, $httpcode);
	}

	public function test302()
	{
		curl_setopt_array($ch = curl_init('http://localhost:'.self::$port.'/redir'), [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 3,
		]);
		$response = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$this->assertEquals('y', $response);
		$this->assertEquals(302, $httpcode);
	}

	public function test200()
	{
		curl_setopt_array($ch = curl_init('http://localhost:'.self::$port.'/hello'), [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 3,
		]);
		$response = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$this->assertEquals('y', $response);
		$this->assertEquals(200, $httpcode);
	}
}
