<?php

declare(strict_types=1);
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Tests\ServerTests;

use Dromos\Tests\AbstractDromosServerTest;

/**
 * @internal
 * @coversNothing
 */
final class Error5058ServerTest extends AbstractDromosServerTest
{
	public function test500()
	{
		curl_setopt_array($ch = curl_init('http://localhost:'.self::$port.'/hello/world'), [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 3,
		]);
		$response = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$this->assertEquals('500', $response);
		$this->assertEquals(500, $httpcode);
	}
}
