<?php

declare(strict_types=1);
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Tests;

use Dromos\Dromos;

/**
 * @internal
 * @coversNothing
 */
final class DromosTest extends AbstractDromosTest
{
	public function testConstructor(): void
	{
		$dromos_app = new Dromos();

		$this->assertNotNull($dromos_app);
		$this->assertTrue($dromos_app instanceof Dromos);
	}
}
