<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Tests;

use Dromos\Dromos;
use Dromos\Exceptions\ValidationException;
use Dromos\Request;
use Dromos\Response;
use Dromos\ServiceProvider;
use Dromos\Validator;

/**
 * @internal
 * @coversNothing
 */
class ServiceProviderTest extends AbstractDromosTest
{
	public function testMarkdownParser()
	{
		// Test basic markdown conversion
		$this->assertSame(
			'<strong>dog</strong> <em>cat</em> <a href="src">name</a>',
			ServiceProvider::markdown('**dog** *cat* [name](src)')
		);

		// Test array arguments
		$this->assertSame(
			'<strong>huh</strong> <em>12</em> <strong>CD</strong>',
			ServiceProvider::markdown('**%s** *%d* **%X**', ['huh', '12', 205])
		);

		// Test variable number of arguments
		$this->assertSame(
			'<strong>huh</strong> <em>12</em> <strong>CD</strong>',
			ServiceProvider::markdown('**%s** *%d* **%X**', 'huh', '12', 205)
		);

		// Test second array argument overrides other arguments
		$this->assertSame(
			'<strong>huh</strong> <em>12</em> <strong>CD</strong>',
			ServiceProvider::markdown('**%s** *%d* **%X**', ['huh', '12', 205], 'dog', 'cheese')
		);
	}

	public function testEscapeCharacters()
	{
		$this->assertSame(
			'H&egrave;&egrave;&egrave;llo! A&amp;W root beer is now 20% off!!',
			ServiceProvider::escape('Hèèèllo! A&W root beer is now 20% off!!')
		);
	}

	public function testRefresh()
	{
		$this->dromos_app->respond(
			function ($request, $response, $service) {
				$service->refresh();
			}
		);

		$this->dromos_app->dispatch();

		$this->assertSame(
			$this->dromos_app->request->uri(),
			$this->dromos_app->response->headers->get('location')
		);
		$this->assertFalse($this->dromos_app->response->isSent());

		// Make sure we got a 3xx response code
		$this->assertGreaterThan(299, $this->dromos_app->response->code());
		$this->assertLessThan(400, $this->dromos_app->response->code());
	}

	public function testBack()
	{
		$url = 'http://google.com/';

		$request = new Request();
		$request->server->set('HTTP_REFERER', $url);

		$dromos_app = new Dromos($request);

		$dromos_app->respond(
			function ($request, $response, $service) {
				$service->back();
			}
		);

		$dromos_app->dispatch();

		$this->assertSame(
			$url,
			$dromos_app->response->headers->get('location')
		);
		$this->assertFalse($dromos_app->response->isSent());

		// Make sure we got a 3xx response code
		$this->assertGreaterThan(299, $dromos_app->response->code());
		$this->assertLessThan(400, $dromos_app->response->code());
	}

	public function testBackWithoutRefererSet()
	{
		$request = new Request();

		$dromos_app = new Dromos($request);

		$dromos_app->respond(
			function ($request, $response, $service) {
				$service->back();
			}
		);

		$dromos_app->dispatch($request);

		$this->assertFalse($dromos_app->response->isSent());

		// Make sure we got a 3xx response code
		$this->assertGreaterThan(299, $dromos_app->response->code());
		$this->assertLessThan(400, $dromos_app->response->code());
	}

	public function testAddValidator()
	{
		$service = new ServiceProvider();

		// Initially empty
		$this->assertEmpty(Validator::$methods);

		$test_callback = function () {
			echo 'test';
		};

		$service->addValidator('awesome', $test_callback);

		$this->assertNotEmpty(Validator::$methods);
		$this->assertArrayHasKey('awesome', Validator::$methods);
		$this->assertContains($test_callback, Validator::$methods);
	}

	public function testValidate()
	{
		$this->expectException(ValidationException::class);

		$this->dromos_app->onError(
			function ($exception) {
				throw $exception;
			}
		);

		$this->dromos_app->respond(
			function ($request, $response, $service) {
				$service->validate('thing')->isLen(3);
			}
		);

		$this->dromos_app->dispatch();
	}

	public function testValidateParam()
	{
		$this->expectException(ValidationException::class);

		$this->dromos_app->onError(
			function ($exception) {
				throw $exception;
			}
		);

		$this->dromos_app->respond(
			function ($request, $response, $service) {
				// Set a test param
				$request->paramsNamed->set('name', 'trevor');

				$service->validateParam('name')->notNull()->isLen(3);
			}
		);

		$this->dromos_app->dispatch();
	}

	// Test ALL of the magic setter, getter, exists, and removal methods
	public function testMagicGetSetExistsRemove()
	{
		$test_data = [
			'name' => 'huh?',
		];

		$service = new ServiceProvider();

		$this->assertEmpty($service->sharedData()->all());
		$this->assertNull($service->sharedData()->get('test_data'));
		$this->assertNull($service->name);
		$this->assertFalse(isset($service->name));

		$service->name = $test_data['name'];

		$this->assertTrue(isset($service->name));
		$this->assertSame($test_data['name'], $service->name);

		unset($service->name);

		$this->assertEmpty($service->sharedData()->all());
		$this->assertNull($service->sharedData()->get('test_data'));
		$this->assertNull($service->name);
		$this->assertFalse(isset($service->name));
	}
}
