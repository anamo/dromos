<?php

declare(strict_types=1);
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Tests;

/**
 * @internal
 * @coversNothing
 */
final class RoutingTest extends AbstractDromosTest
{
	public function testBasic(): void
	{
		$this->expectOutputString('x');

		$this->dromos_app->respond('/^.*$/', function (&$request, &$response) {
			$response->body('x');
			$response->send();
		});

		$this->dromos_app->dispatch();
	}
}
