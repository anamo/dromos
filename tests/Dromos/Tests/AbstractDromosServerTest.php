<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Tests;

use Symfony\Component\Process\Process;

abstract class AbstractDromosServerTest extends AbstractDromosTest
{
	protected static Process $process;
	protected static string $path;
	protected static int $port;

	public static function setUpBeforeClass(): void
	{
		self::$path = strtolower(preg_replace('/(?<!^)([A-Z])/', '-$1', str_replace('_', '-', basename(static::class))));
		self::$port = (int) filter_var(self::$path, FILTER_SANITIZE_NUMBER_INT);

		self::$process = new Process(['php', '-S', 'localhost:'.self::$port, '-d output_buffering=Off', '-d zlib.output_compression=Off', '-d display_errors=1', '-d error_reporting=32767', 'tests/server/'.self::$path.'.php']);
		self::$process->start();

		sleep(1);
	}

	public static function tearDownAfterClass(): void
	{
		self::$process->stop();
	}
}
