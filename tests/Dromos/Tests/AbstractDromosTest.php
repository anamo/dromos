<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

namespace Dromos\Tests;

use Dromos\Dromos;
use Dromos\Tests\Mocks\MockRequestFactory;
use PHPUnit\Framework\TestCase;

abstract class AbstractDromosTest extends TestCase
{
	protected Dromos $dromos_app;

	protected function setUp(): void
	{
		$mock_request = MockRequestFactory::create('/');

		$dromos = new Dromos(
			$mock_request
		);

		$this->dromos_app = new Dromos();
	}
}
