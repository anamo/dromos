<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

// php -S localhost:5055 -d output_buffering=Off -d zlib.output_compression=Off -d display_errors=1 -d error_reporting=32767 tests/server/exception5055-server-test.php

use Dromos\Dromos;
use Dromos\Log\MonologTelegramHandler;
use Dromos\Request;
use Dromos\Response;
use Monolog\Logger;

(function () {
	$path = '/';
	while (!($autoloader = @include __DIR__."{$path}/vendor/autoload.php")) {
		$path .= '../';
	}
})();

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
	throw new ErrorException(@$errstr, 0, @$errno, @$errfile, @$errline);
});

set_exception_handler($bootstrap_exception_handler = function (Throwable $ex) {
	curl_setopt_array($ch = curl_init(), [
		CURLOPT_URL => 'https://api.telegram.org/bot178153406:AAEmBXf4gjvO577bp25eBXKhDCPnKyBJ0Iw/sendMessage?'.http_build_query(['chat_id' => '178163713', 'text' => $ex->getMessage()]),
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_SSL_VERIFYPEER => 1,
		CURLOPT_TIMEOUT => 1,
	]);
	$output = curl_exec($ch);
	curl_close($ch);
});

$dromos = new Dromos();

$dromos->app->register('log', function () {
	$log = new Logger('telegram-logger');
	$log->pushHandler(new MonologTelegramHandler('bot178153406:AAEmBXf4gjvO577bp25eBXKhDCPnKyBJ0Iw', '178163713', Logger::DEBUG));

	return $log;
});

$dromos->onError(function (Throwable &$ex, &$dromos) use (&$bootstrap_exception_handler) {
	try {
		// log
		$dromos->app->log->error($ex->getMessage());

		// output
		$dromos->response->body('500');
		$dromos->response->code(500);
		$dromos->response->send(true);
	} catch (Throwable $ex) {
		restore_exception_handler();
		$bootstrap_exception_handler($ex);
	}
});

$dromos->afterDispatch(function (&$dromos, &$request, &$response, &$service, &$app) {
	// log
	$app->log->info($response->status());
});

$dromos->afterDispatch(function (&$dromos, &$request, &$response) {
	if ($response->isSent()) {
		return;
	}

	if (empty($response->body())) {
		$response->body('404');
		$response->code(404);
	}

	$response->send();
});

$dromos->get('/^\/exc$/', function (Request &$request, Response &$response) {
	throw new ErrorException('This is a ErrorException message', 0, E_USER_WARNING);
});

$dromos->respond('GET', '/^\/err$/', function (Request &$request, Response &$response) {
	trigger_error('This is a trigger_error message', E_USER_WARNING);
});

$dromos->respond('/^.*$/', function (Request &$request, Response &$response) {
	$response->body('x');
});

$dromos->respond('/^\/redir$/', function (Request &$request, Response &$response, &$service) {
	$response->redirect(
		'/redirected'
	);
});

$dromos->respond('/^.*$/', function (Request &$request, Response &$response) use (&$dromos) {
	$response->body('y');

	$dromos->skipRemaining();
});

$dromos->respond('/^.*$/', function (Request &$request, Response &$response) {
	$response->body('z');
});

$dromos->dispatch();
