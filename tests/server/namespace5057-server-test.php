<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

// php -S localhost:5057 -d output_buffering=Off -d zlib.output_compression=Off -d display_errors=1 -d error_reporting=32767 tests/server/namespace-5057.php

use Dromos\Dromos;
use Dromos\Log\MonologTelegramHandler;
use Dromos\Request;
use Dromos\Response;
use Monolog\Logger;

(function () {
	$path = '/';
	while (!($autoloader = @include __DIR__."{$path}/vendor/autoload.php")) {
		$path .= '../';
	}
})();

$dromos = new Dromos();

$dromos->app->register('log', function () {
	$log = new Logger('telegram-logger');
	$log->pushHandler(new MonologTelegramHandler('bot178153406:AAEmBXf4gjvO577bp25eBXKhDCPnKyBJ0Iw', '178163713', Logger::DEBUG));

	return $log;
});

$dromos->afterDispatch(function (&$dromos, &$request, &$response, &$service, &$app) {
	// log
	$app->log->info($response->status());
});

$dromos->with('/foo', function () use (&$dromos) {
	$dromos->with('/bar', function () use (&$dromos) {
		$dromos->respond('/^\/hello$/', function (Request &$request, Response &$response) {
			$response->body('hello');
		});
	});
});

$dromos->afterDispatch(function (&$dromos, &$request, &$response) {
	if ($response->isSent()) {
		return;
	}

	if (empty($response->body())) {
		$response->body('404');
		$response->code(404);
	}

	$response->send();
});

$dromos->dispatch();
