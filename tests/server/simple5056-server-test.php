<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos

// php -S localhost:5056 -d output_buffering=Off -d zlib.output_compression=Off -d display_errors=1 -d error_reporting=32767 tests/server/simple5056-server-test.php

use Dromos\Dromos;
use Dromos\Request;
use Dromos\Response;

(function () {
	$path = '/';
	while (!($autoloader = @include __DIR__."{$path}/vendor/autoload.php")) {
		$path .= '../';
	}
})();

$dromos = new Dromos();

$dromos->respond('/^\/hello$/', function (Request &$request, Response &$response) {
	$response->body('hello');
});

$dromos->afterDispatch(function (&$dromos) {
	if ($dromos->response->isSent()) {
		return;
	}

	if (empty($dromos->response->body())) {
		$dromos->response->body('404');
		$dromos->response->code(404);
	}

	$dromos->response->send();
});

$dromos->dispatch();
