<?php
// © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/php-json-api-server

$finder = PhpCsFixer\Finder::create()
	->exclude('vendor')
	->in(__DIR__)
;

$config = new PhpCsFixer\Config();

return $config
	->setRules([
		'@PhpCsFixer' => true,
		'header_comment' => ['header' => '© 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/dromos', 'comment_type' => 'comment', 'location' => 'after_declare_strict', 'separate' => 'bottom'],
		'modernize_types_casting' => true,
		'self_accessor' => true,
		'yoda_style' => ['equal' => true, 'identical' => true, 'less_and_greater' => true],
	])
	->setIndent("\t")
	->setFinder($finder)
;
